ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/json-serde-1.3.8-jar-with-dependencies.jar;

USE rogozhevaan;

DROP VIEW IF EXISTS first_part;
CREATE VIEW first_part AS
SELECT content.userInn as inn, AVG(COALESCE(content.totalSum, 0)) as average_profit
FROM kkt_table
WHERE HOUR(content.datetime.date) * 60 + MINUTE(content.datetime.date) < 13*60
GROUP BY content.userInn;

DROP VIEW IF EXISTS second_part;
CREATE VIEW second_part AS
SELECT content.userInn as inn, AVG(COALESCE(content.totalSum, 0)) as average_profit
FROM kkt_table
WHERE HOUR(content.datetime.date) * 60 + MINUTE(content.datetime.date) >= 13 * 60
GROUP BY content.userInn;

SELECT f.inn as inn, ROUND(f.average_profit) as first_average_profit, ROUND(s.average_profit) as second_average_profit
FROM first_part f
LEFT JOIN second_part s
ON f.inn = s.inn
WHERE f.average_profit > s.average_profit
ORDER BY first_average_profit
LIMIT 50;
